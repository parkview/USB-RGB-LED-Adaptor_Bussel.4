Project Name:  USB RGB LED Adaptor - Bussel.4
Initiation Date:  21-04-06


James uses a conglomerate of cables to help connect Microbit to a USB powered string of RGB WS2812 LEDs.  
I thought it might make sense, or be easier to use a small PCB.

Project could be renamed to:  Microbit/ESP32 USB RGB LED Adaptor PCB, but we settled on the: Bussel.4




Useful URLs:
------------


Hardware:
---------
1 x USB-Micro for Power input
1 x USB A for Power out (no data connections)
2 x JST-PH connectors for RGB Power and Data out
1 x black JST cable solder pads Power and data out
2 x pins for RGB Data input from Microbit
1 x solder strip for soldering LED strip to back of PCB


PCB Versions:
-------------
Version 1.0:  
Production Date:  2021-04-30
PCB Details:  Bussel.4, JLCPCB, 1.6mm, white PCB, 2 layer

Done: Add in a 3.3V to 5V level translator IC SN74LVC1....
Done: add 22uF + 2uF caps to PH out connectors


Version 0.5:
Production Date:  2021-04-11
PCB Details:  JLCPCB, White 1.6mm, 2 layer

2nd version.  Bussel.1 = USB at either end of PCB, but James says it would help with cable management if they both came out on the same side.  This led onto the Bussel.3.  2 x PH connectors on one side and SUB-A, USB-Micro and dupont connector on the other side.  Solder down LED strip pads where removed.
Componants ordered via LCSC.



Code Information:
-----------------
N/A


Project Journal:
----------------
2021-04-30:  PCB Gerber files sent off for manufacture.
2021-04-29:  Finished V1.0 re-design.  This does data 3V to 5V voltage level translation
2021-04-08:  Finished designing up PCB V0.5.  This was Bussel.1, which after a redesign became Bussel.3 (no data voltage translation)


































